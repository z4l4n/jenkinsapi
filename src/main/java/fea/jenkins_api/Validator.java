package fea.jenkins_api;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Validator {

	public static void validateStringIsNotNullOrEmpty(String str, String nameOfstr) {
		if (str == null || str.length() == 0) {
			throw new IllegalArgumentException("The " + nameOfstr + " string can't be null or empty string!");
		}
	}

	public static void validateUrl(String jenkinsURLStr) {
		HttpURLConnection c;
		URL url;
		try {
			url = new URL(jenkinsURLStr);
			c = (HttpURLConnection) url.openConnection();
			c.connect();
			String s = c.getHeaderField("X-Jenkins");
			if (s == null) {
				throw new IllegalArgumentException("The argument jenkingURLStr is not an URL of a Jenkins server!");
			}
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("The argument jenkinsURLStr is not in a valid URL format!");
		} catch (IOException e) {
			throw new RuntimeException("Some IO error has occured!");
		}
	}

}
