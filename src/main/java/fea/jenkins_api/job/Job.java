package fea.jenkins_api.job;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fea.jenkins_api.Validator;

public class Job {

	// private final String id;
	private final String name;
	private String description;
	private BuildDiscarder bDiscarder;
	private Filter filter;
	private Language language = new Language();
	private boolean continueBuildIfFailed;
	private boolean enabled = true;
	private boolean vulnerabilityCheck;
	private Git git = new Git();
	private Trigger trigger = new Trigger();
	private Document doc;
	// private BuildSystem buildSystem = BuildSystem.NONE;

	public Job(String name) {
		// this.id = id;
		Validator.validateStringIsNotNullOrEmpty(name, "name");
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 *
	 * @return A Git object where you can set the Git related options
	 */
	public Git getGit() {
		return this.git;
	}

	/**
	 * Enables trigger(s) on this job
	 *
	 * @return a Trigger object where you can set trigger options
	 */
	public Trigger getTrigger() {
		return this.trigger;
	}

	public Language getLanguage() {
		return this.language;
	}

	public BuildDiscarder getBuildDiscarder() {
		if (bDiscarder == null) {
			this.bDiscarder = new BuildDiscarder();
		}
		return this.bDiscarder;
	}

	public Filter getFilter() {
		if (this.filter == null) {
			this.filter = new Filter();
		}

		return this.filter;
	}

	/**
	 *
	 * If this is true, in case of any build error the build will be continue with
	 * the next revision.
	 */
	public void setContinueBuildIfFailed(boolean continueBuildIfFailed) {
		this.continueBuildIfFailed = continueBuildIfFailed;
	}

	/**
	 * Here you can disable the VulnerabilityHunter module, if you dont have the
	 * time to running it. Otherwise it is highly recommended to leave it enabled.
	 */
	public void setVulnerabilityCheck(boolean vulnerabilityCheck) {
		this.vulnerabilityCheck = vulnerabilityCheck;
	}

	/**
	 *
	 * When this is set to false, no new builds of this project will be executed.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Sets a description about this job
	 *
	 */
	public void setDescription(String description) {
		Validator.validateStringIsNotNullOrEmpty(description, "description");
		this.description = description;
	}

	public void clearFilter() {
		this.filter = null;
	}

	public void clearBuildDiscarder() {
		this.bDiscarder = null;
	}

	public void clearDescription() {
		this.description = null;
	}

	/*
	 * public void setBuildSystem(BuildSystem buildSystem) { this.buildSystem =
	 * buildSystem; }
	 */

	/**
	 * Returns the job's FEA-Jenkins plugin config.xml representation in a byte
	 * array
	 *
	 * @return byte array of the this job's config.xml
	 */
	public byte[] toXML() {
		try {
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("Couldn't create a DocumentBuilder...");
		}

		Element rootElem, elem1, elem2;

		rootElem = appendChild(doc, "com.frontendart.plugin.feasourcecodeanalyzer.FEASourceCodeAnalyzer");

		appendChild(rootElem, "actions");

		appendChild(rootElem, "description").setNodeValue(description);

		if (bDiscarder != null) {
			elem1 = appendChildWithAttribute(rootElem, "logRotator", "class", "hudson.tasks.LogRotator");
			appendChildWithTextContent(elem1, "daysToKeep", String.valueOf(bDiscarder.getDaysToKeep()));
			appendChildWithTextContent(elem1, "numToKeep", String.valueOf(bDiscarder.getNumToKeep()));
			appendChildWithTextContent(elem1, "artifactDaysToKeep", String.valueOf(bDiscarder.getArtifactDaysToKeep()));
			appendChildWithTextContent(elem1, "artifactNumToKeep", String.valueOf(bDiscarder.getArtifactNumsToKeep()));
		}

		appendChildWithTextContent(rootElem, "keepDependencies", "false");

		appendChild(rootElem, "properties");

		appendChildWithAttribute(rootElem, "scm", "class", "hudson.scm.NullSCM");

		appendChildWithTextContent(rootElem, "canRoam", "true");

		appendChildWithTextContent(rootElem, "disabled", enabled ? "false" : "true");

		appendChildWithTextContent(rootElem, "blockBuildWhenDownstreamBuilding", "false");

		appendChildWithTextContent(rootElem, "blockBuildWhenUpstreamBuilding", "false");

		elem1 = appendChild(rootElem, "triggers"); // !!!
		if (trigger.getPeriodicBuild() != null) {
			if (trigger.getPeriodicBuild().getSchedule() != null) {
				appendChildWithTextContent(appendChild(elem1, "hudson.triggers.TimerTrigger"), "spec",
						trigger.getPeriodicBuild().getSchedule());
			} else {
				appendChild(appendChild(elem1, "hudson.triggers.TimerTrigger"), "spec");
			}
		}

		appendChildWithTextContent(rootElem, "concurrentBuild", "false");

		elem1 = appendChildWithAttribute(appendChild(rootElem, "builders"), "hudson.tasks.Ant", "plugin", "ant@1.7");
		appendChild(elem1, "targets");
		appendChildWithTextContent(elem1, "antName", "Ant");
		appendChildWithTextContent(elem1, "buildFile", "/opt/SourceMeter/toolchain.xml");
		appendChildWithTextContent(elem1, "properties", "packDir=/opt/toolkit");

		appendChild(rootElem, "publishers");

		appendChild(rootElem, "buildWrappers");

		// START OF form element
		elem1 = appendChild(rootElem, "form");
		appendChildWithAttribute(elem1, "project", "reference", "../..");
		appendChild(elem1, "remiurl");
		// appendChildWithTextContent(elem1, "remiuniquename", id);
		appendChildWithTextContent(elem1, "jobname", name);
		appendChild(elem1, "svnroot");
		appendChild(elem1, "svnusername");
		appendChildWithTextContent(elem1, "svnpassword", "+YpobtFirOCszgutbY5+Jg==");
		appendChild(elem1, "svnselectedrevisions");
		appendChild(elem1, "cvsusername");
		appendChildWithTextContent(elem1, "cvspassword", "+YpobtFirOCszgutbY5+Jg==");
		appendChildWithTextContent(elem1, "cvsport", "0");
		appendChild(elem1, "cvsselectedrevisions");
		appendChild(elem1, "tfvcusername");
		appendChildWithTextContent(elem1, "tfvcpassword", "+YpobtFirOCszgutbY5+Jg==");
		appendChild(elem1, "tfvcselectedrevisions");

		if (filter != null) {
			appendChild(elem1, "filter").setNodeValue(filter.getHardFilter());
			appendChild(elem1, "softFilter").setNodeValue(filter.getSoftFilter());
		} else {
			appendChild(elem1, "filter").setNodeValue(null);
			appendChild(elem1, "softFilter").setNodeValue(null);
		}

		appendChildWithTextContent(elem1, "doreport", "false");

		appendChildWithTextContent(elem1, "dofilter", filter == null ? "false" : "true");

		appendChildWithTextContent(elem1, "dobenchmark", "false");

		appendChildWithTextContent(elem1, "disablevulnerability", String.valueOf(!vulnerabilityCheck)); // !!!!

		appendChildWithTextContent(elem1, "continueifbuildfailed", String.valueOf(continueBuildIfFailed));

		appendChildWithTextContent(elem1, "disablesimbolic", "false"); // ??

		elem2 = appendChild(elem1, "dovcs");
		appendChildWithTextContent(elem2, "boolean", "false");
		appendChildWithTextContent(elem2, "boolean", "false");
		appendChildWithTextContent(elem2, "boolean", "false");
		appendChildWithTextContent(elem2, "boolean", "true");
		appendChildWithTextContent(elem2, "boolean", "false");

		elem2 = appendChild(elem1, "dovcslist");
		appendChildWithTextContent(elem2, "com.frontendart.plugin.form.VCS", "SVN");
		appendChildWithTextContent(elem2, "com.frontendart.plugin.form.VCS", "CVS");
		appendChildWithTextContent(elem2, "com.frontendart.plugin.form.VCS", "TFVC");
		appendChildWithTextContent(elem2, "com.frontendart.plugin.form.VCS", "GIT");
		appendChildWithTextContent(elem2, "com.frontendart.plugin.form.VCS", "FOLDER");

		appendChildWithTextContent(elem1, "usedVCS", "GIT");

		appendChild(elem1, "modelnames");

		appendChild(elem1, "modelcheck");

		appendChild(elem1, "modelinfos");

		appendChild(elem1, "svnbranches");

		appendChild(elem1, "cvsbranches");

		appendChild(elem1, "cvsmodules");

		appendChildWithTextContent(elem1, "tfvcbranches", "");

		appendChildWithTextContent(elem1, "tfvcmodules", "");

		appendChildWithTextContent(elem1, "gitbranches", git.getBranch());

		appendChildWithTextContent(elem1, "gitrepos", git.getRepoURL());

		appendChildWithTextContent(elem1, "gitusers", git.getAuthentication().getUsername());

		appendChildWithTextContent(elem1, "gitpasswords", git.getAuthentication().getPassword());

		appendChildWithTextContent(elem1, "gitprivatekey", git.getAuthentication().getPrivateKey());

		appendChildWithTextContent(elem1, "gitknownhosts", "");

		appendChildWithTextContent(elem1, "gitpassphrase", git.getAuthentication().getPassphrase());

		appendChildWithTextContent(elem1, "gitintervalstart", git.getGitIntervalStart());

		appendChildWithTextContent(elem1, "gitintervalstart", git.getGitIntervalEnd());

		appendChildWithTextContent(elem1, "gitseries", "");

		appendChildWithTextContent(elem1, "folderurl", "");

		appendChildWithTextContent(elem1, "reportdir", "");

		appendChildWithTextContent(elem1, "revision", "");

		appendChildWithTextContent(elem1, "year", "");

		appendChildWithTextContent(elem1, "month", "");

		appendChildWithTextContent(elem1, "day", "");

		appendChildWithTextContent(elem1, "hour", "");

		appendChildWithTextContent(elem1, "minute", "");

		appendChildWithTextContent(elem1, "sec", "");

		appendChildWithTextContent(elem1, "config", "/var/jenkins_home/jobs/" + name + "/ws/jenkinsconfig.properties"); // ???
		// END OF form element

		// Language JAVA wired...
		elem1 = appendChildWithAttribute(rootElem, "language", "class",
				"com.frontendart.plugin.beans.language.JavaLanguage");
		appendChild(elem1, "properties");
		appendChildWithTextContent(elem1, "staplerClass", "com.frontendart.plugin.beans.language.JavaLanguage");

		// Build NONE wired...
		elem1 = appendChildWithAttribute(rootElem, "build", "class", "com.frontendart.plugin.beans.build.NoneBuild");
		appendChildWithTextContent(elem1, "staplerClass", "com.frontendart.plugin.beans.build.NoneBuild");

		elem1 = appendChildWithAttribute(rootElem, "projectmanagement", "class",
				"com.frontendart.plugin.beans.projectmanagement.NoneProjectManagement");
		appendChild(elem1, "properties");
		appendChildWithTextContent(elem1, "staplerClass",
				"com.frontendart.plugin.beans.projectmanagement.NoneProjectManagement");

		appendChildWithTextContent(rootElem, "projectmanager", "");

		appendChildWithTextContent(rootElem, "publicproject", "false");

		appendChildWithTextContent(rootElem, "developer", "");

		appendChildWithTextContent(rootElem, "critical", "false");

		appendChildWithTextContent(rootElem, "blocker", "false");

		appendChildWithTextContent(rootElem, "legacy", "false");

		// transforming the DOM to XML
		TransformerFactory tFactory = TransformerFactory.newInstance();

		Transformer transformer = null;
		try {
			transformer = tFactory.newTransformer();
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DOMSource source = new DOMSource(doc);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(out);
		try {
			transformer.transform(source, result);
		} catch (TransformerException e1) {
			throw new RuntimeException("Couldn't transform to xml!");
		}

		return out.toByteArray();
	}

	// just for testing
	void printToFile(String filePath) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(toXML())));
				PrintWriter pw = new PrintWriter(filePath);) {
			String s;
			while ((s = br.readLine()) != null) {
				pw.println(s);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Element appendChild(Node parent, String childName) {
		Element e = doc.createElement(childName);
		parent.appendChild(e);
		return e;
	}

	private Element appendChildWithTextContent(Node parent, String childName, String textContent) {
		Element e = doc.createElement(childName);
		e.setTextContent(textContent);
		parent.appendChild(e);
		return e;
	}

	private Element appendChildWithAttribute(Node parent, String childName, String attribute, String value) {
		Element e = doc.createElement(childName);
		e.setAttribute(attribute, value);
		parent.appendChild(e);
		return e;
	}

}
