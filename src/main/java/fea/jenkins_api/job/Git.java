package fea.jenkins_api.job;

import java.net.MalformedURLException;
import java.net.URL;

import fea.jenkins_api.Validator;

public class Git {
	private String repoURL;
	private String branch;
	private GitAuthentication authentication = new GitAuthentication();
	private String gitIntervalStart;// "1990.01.01 06:00:00"; // it seems wired into the plugin..
	private String gitIntervalEnd; // "HEAD" so does it

	// <hiddenintervaltype>EVERY</hiddenintervaltype> the default git build
	// frequency doesnt appear in the generated xml..

	public void setRepoURL(String repoURL) {
		Validator.validateStringIsNotNullOrEmpty(repoURL, "repo URL");
		try {
			new URL(repoURL);
			this.repoURL = repoURL;
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("The repoURL is not a valid URL!");
		}
	}

	public void setGitIntervalStart(String gitIntervalStart) {
		Validator.validateStringIsNotNullOrEmpty(gitIntervalStart, "git interval start");
		this.gitIntervalStart = gitIntervalStart;
	}

	public void setGitIntervalEnd(String gitIntervalEnd) {
		Validator.validateStringIsNotNullOrEmpty(gitIntervalEnd, "git interval end");
		this.gitIntervalEnd = gitIntervalEnd;
	}

	public void setBranch(String branch) {
		Validator.validateStringIsNotNullOrEmpty(branch, "branch");
		this.branch = branch;
	}

	public void clearGitIntervalStart() {
		gitIntervalStart = null;
	}

	public void clearGitIntervalend() {
		gitIntervalEnd = null;
	}

	public void clearBranch() {
		branch = null;
	}

	public void clearRepoURL() {
		repoURL = null;
	}

	public String getRepoURL() {
		return repoURL;
	}

	public String getBranch() {
		return branch;
	}

	public GitAuthentication getAuthentication() {
		return authentication;
	}

	public String getGitIntervalStart() {
		return gitIntervalStart;
	}

	public String getGitIntervalEnd() {
		return gitIntervalEnd;
	}

}
