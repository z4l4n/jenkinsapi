package fea.jenkins_api.job;

public enum BuildSystem {
	NONE, MAVEN, ANT
}
