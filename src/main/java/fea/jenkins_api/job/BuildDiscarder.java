package fea.jenkins_api.job;

public class BuildDiscarder {

	private int daysToKeep = -1;
	private int numToKeep = -1;
	private int artifactDaysToKeep = -1;
	private int artifactNumsToKeep = -1;

	public void setDaysToKeep(int value) {
		if (value < 1 && value != -1) {
			throw new IllegalArgumentException("The value must be a positive integer or -1!");
		}
		this.numToKeep = value;
	}

	public void setNumToKeep(int value) {
		if (value < 1 && value != -1) {
			throw new IllegalArgumentException("The value must be a positive integer or -1!");
		}
		this.numToKeep = value;
	}

	public void setArtifactDaysToKeep(int value) {
		if (value < 1 && value != -1) {
			throw new IllegalArgumentException("The value must be a positive integer or -1!");
		}
		this.artifactDaysToKeep = value;
	}

	public void setArtifactNumsToKeep(int value) {
		if (value < 1 && value != -1) {
			throw new IllegalArgumentException("The value must be a positive integer or -1!");
		}
		this.artifactNumsToKeep = value;
	}

	public int getDaysToKeep() {
		return daysToKeep;
	}

	public int getNumToKeep() {
		return numToKeep;
	}

	public int getArtifactDaysToKeep() {
		return artifactDaysToKeep;
	}

	public int getArtifactNumsToKeep() {
		return artifactNumsToKeep;
	}

}
