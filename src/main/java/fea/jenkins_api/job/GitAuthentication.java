package fea.jenkins_api.job;

import fea.jenkins_api.Validator;

public class GitAuthentication {

	private String username;
	private String password;
	private String privateKey;
	private String passphrase;

	public void setUsername(String username) {
		Validator.validateStringIsNotNullOrEmpty(username, "username");
		this.username = username;
	}

	public void setPassword(String password) {
		Validator.validateStringIsNotNullOrEmpty(password, "password");
		this.password = password;
	}

	public void setPrivateKey(String privateKey) {
		Validator.validateStringIsNotNullOrEmpty(privateKey, "private key");
		this.privateKey = privateKey;
	}

	public void setPassphrase(String passphrase) {
		Validator.validateStringIsNotNullOrEmpty(passphrase, "passphrase");
		this.passphrase = passphrase;
	}

	public void clearUsername() {
		username = null;
	}

	public void clearPassword() {
		password = null;
	}

	public void clearPrivateKey() {
		privateKey = null;
	}

	public void clearPassphrase() {
		passphrase = null;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public String getPassphrase() {
		return passphrase;
	}

}
