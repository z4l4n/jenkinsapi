package fea.jenkins_api.job;

import fea.jenkins_api.Validator;

public class Filter {

	private String hardFilter;
	private String softFilter;

	public void setHardFilter(String hardFilter) {
		Validator.validateStringIsNotNullOrEmpty(hardFilter, "hard filter");

		this.hardFilter = hardFilter;
	}

	public void setSoftFilter(String softFilter) {
		Validator.validateStringIsNotNullOrEmpty(softFilter, "soft filter");

		this.softFilter = softFilter;
	}

	public void clearHardFilter() {
		hardFilter = null;
	}

	public void clearSoftFilter() {
		softFilter = null;
	}

	public String getHardFilter() {
		return hardFilter;
	}

	public String getSoftFilter() {
		return softFilter;
	}

}
