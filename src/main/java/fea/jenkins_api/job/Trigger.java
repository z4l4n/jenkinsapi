package fea.jenkins_api.job;

import fea.jenkins_api.Validator;

public class Trigger {

	private PeriodicBuild periodicBuild;

	public PeriodicBuild getPeriodicBuild() {
		if (periodicBuild == null) {
			periodicBuild = new PeriodicBuild();
		}
		return periodicBuild;
	}

	public void disablePeriodicBuild() {
		periodicBuild = null;
	}

	public class PeriodicBuild {
		private String schedule;

		public void setSchedule(String schedule) {
			Validator.validateStringIsNotNullOrEmpty(schedule, "schedule");
			this.schedule = schedule;
		}

		public void clearSchedule() {
			this.schedule = null;
		}

		String getSchedule() {
			return schedule;
		}

	}
}
