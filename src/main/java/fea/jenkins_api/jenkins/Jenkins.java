package fea.jenkins_api.jenkins;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fea.jenkins_api.Validator;
import fea.jenkins_api.job.Job;

public final class Jenkins {

    private final String jenkinsURL;
    private final Charset utf8 = Charset.forName("UTF-8");

    /**
     * Creates a JenkinsAPI object which represents a running Jenkins server and can
     * be used to execute certain Jenkins API calls. The jenkinsURLStr argument must
     * specify a valid URL to the Jenkins server.
     *
     * @param jenkinsURLStr
     *            A valid URL to the Jenkins server
     * @throws IllegalArgumentException
     *             If the given URL isn't in valid URL format or it's not a Jenkins
     *             URL
     */
    public Jenkins(String jenkinsURLStr) {
	if (!jenkinsURLStr.endsWith("/")) {
	    jenkinsURLStr = jenkinsURLStr + "/";
	}
	Validator.validateUrl(jenkinsURLStr);
	this.jenkinsURL = jenkinsURLStr;
    }

    /**
     * Returns the result of an XPath counter function through Jenkins API call
     *
     * @param APICall
     *            Jenkins API call, relative to the Jenkins' URL. e.g.:
     *            'job/jobName/xml/&xpath=count(/element)
     */
    private int countQuery(String APICall) {
	int result = -1;
	HttpResponse r;
	int statusCode = 0;
	try {
	    r = Request.Get(jenkinsURL + APICall).execute().returnResponse();
	    statusCode = r.getStatusLine().getStatusCode();
	    if (statusCode != 200) {
		throw new RuntimeException("Some error has happened, the returned status code: " + statusCode);
	    }
	    try (BufferedReader br = new BufferedReader(new InputStreamReader(r.getEntity().getContent(), utf8))) {
		String s = br.readLine();
		if (s == null) {
		    throw new RuntimeException("The response doesn't have message body!");
		}
		result = (int) Float.parseFloat(s);

	    } catch (NumberFormatException e) {
		throw new RuntimeException("Couldn't parse the result to integer!");
	    }

	} catch (UnsupportedEncodingException e) {
	    throw new RuntimeException("Error during encoding the job's name!");
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}
	return result;
    }

    /**
     * Returns the number of the idle executors on this Jenkins server
     */
    public int getNumberOfIdleExecutors() {

	return countQuery(
		"computer/api/xml?tree=computer[executors[idle]]&xpath=count(/computerSet/computer/executor[idle=%27true%27])");
    }

    /**
     * Returns the number of items in the build queue on this Jenkins server.
     */
    public int getNumberOfItemsInQueue() {

	return countQuery("queue/api/xml?tree=items&xpath=count(/queue/item)");
    }

    /**
     * Returns the URLs of the running jobs on this Jenkins server in a String
     * array.
     *
     * @return A String array which contains the URLs of the running jobs. If there
     *         aren't any jobs the returned value is null
     */
    public List<String> getRunningJobURL() {
	Document doc;
	try {
	    doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(jenkinsURL
		    + "computer/api/xml?tree=computer[executors[currentExecutable[url]]]&xpath=computerSet/computer/executor/currentExecutable/url&wrapper=runningJobs");
	} catch (SAXException | IOException | ParserConfigurationException e) {
	    throw new RuntimeException("Couldn't parse the xml of the running jobs!");
	}

	Element elem = doc.getDocumentElement();

	if (!elem.getNodeName().equals("runningJobs")) {
	    throw new RuntimeException("The fetched xml isn't the expected one!");
	}

	if (!elem.hasChildNodes()) {
	    return null;
	}

	if (!elem.getFirstChild().getNodeName().equals("url")) {
	    throw new RuntimeException("The root element's child node isn't the expected one'<url>...</url>' !");
	}

	NodeList children = elem.getChildNodes();
	List<String> result = new ArrayList<>();

	for (int i = 0; i < children.getLength(); i++) {
	    Node child = children.item(i);

	    if (child.getNodeName().equals("url")) {
		result.add(child.getTextContent());
	    }
	}

	return result.stream().map(n -> {
	    int index = n.indexOf('/', n.indexOf("/job/") + 5);
	    return n.substring(0, index + 1);
	}).collect(Collectors.toList());
    }

    /**
     * Creates a job on the Jenkins server by config.xml file. The name and the
     * properties of the new job are given as arguments.
     *
     * @param jobName
     *            name of the new job to be created
     * @param configXMLPath
     *            path to the corresponding config.xml file
     * @throws IllegalArgumentException
     *             if the config.xml can't be found or the length of the jobName
     *             String is zero
     */
    public void createJob(String jobName, String configXMLPath) {
	Validator.validateStringIsNotNullOrEmpty(jobName, "job name");

	File configXML = new File(configXMLPath);

	if (!configXML.exists()) {
	    throw new IllegalArgumentException("configXMLPath argument isn't a valid path to the config.xml file!");
	}

	String encodedJobName;
	Response r;
	int statusCode = 0;
	try {
	    encodedJobName = URLEncoder.encode(jobName, "utf-8");
	    r = Request.Post(jenkinsURL + "createItem?name=" + encodedJobName)
		    .bodyFile(configXML, ContentType.APPLICATION_XML).execute();
	    statusCode = r.returnResponse().getStatusLine().getStatusCode();
	} catch (UnsupportedEncodingException e) {
	    throw new RuntimeException("Error during encoding the job's name!");
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}

	if (statusCode != 200) {
	    throw new RuntimeException("Some error has happened, the returned status code: " + statusCode);
	}
    }

    /**
     * Creates a job on this Jenkins server from a Job object
     *
     * @param job
     *            a Job object whose config.xml will be used to create a new Jenkins
     *            job
     */
    public void createJob(Job job) {
	String encodedJobName;
	byte[] configXML = job.toXML();
	Response r;
	int statusCode = 0;
	try {
	    encodedJobName = URLEncoder.encode(job.getName(), "utf-8");
	    r = Request.Post(jenkinsURL + "createItem?name=" + encodedJobName)
		    .bodyByteArray(configXML, 0, configXML.length, ContentType.APPLICATION_XML).execute();
	    statusCode = r.returnResponse().getStatusLine().getStatusCode();
	} catch (UnsupportedEncodingException e) {
	    throw new RuntimeException("Error during encoding the job's name!");
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}

	if (statusCode != 200) {
	    throw new RuntimeException("Some error has happened, the returned status code: " + statusCode);
	}
    }

    /**
     * Deletes a job from the Jenkins server by its name. The jobName argument must
     * be an existing job name otherwise exception will be thrown.
     *
     * @param jobName
     *            name of the job to be deleted
     * @throws IllegalArgumentException
     *             if the given job doesn't exist or it's unable to connect to the
     *             URL with that job
     */
    public void deleteJob(String jobName) {
	if (!isExistingJob(jobName)) {
	    throw new IllegalArgumentException("There isn't a job with name '" + jobName + "'!");
	}
	Response r;
	int statusCode = 0;
	try {
	    r = Request.Post(jenkinsURL + "job/" + jobName + "/doDelete").execute();
	    statusCode = r.returnResponse().getStatusLine().getStatusCode();
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}

	if (statusCode == 302) {
	    System.out.println("The job '" + jobName + "' has been deleted!");
	} else {
	    throw new RuntimeException(
		    "The deletion hasn't been performed because some error has happened! Status code: " + statusCode);
	}
    }

    /**
     * Gets a file from a Jenkins job's workspace through an {@link InputStream}.
     * The jobName argument must be an existing job and the pathName argument must
     * be a valid URI of the file, relative to the job's workspace.
     *
     * @param pathName
     *            relative path to the file
     * @param jobName
     *            the name of the existing job whose workspace contains the desired
     *            file
     * @return {@link InputStream} of the file
     * @throws IllegalArgumentException
     *             if the given job or file doesn't exist
     */
    public InputStream getFileFromJobWorkspace(String jobName, String pathName) {
	if (!isExistingJob(jobName)) {
	    throw new IllegalArgumentException("There isn't a job with name '" + jobName + "'!");
	}
	HttpResponse r;
	int statusCode = 0;
	try {
	    r = Request.Get(jenkinsURL + "job/" + jobName + "/ws/" + pathName).execute().returnResponse();
	    statusCode = r.getStatusLine().getStatusCode();
	    if (statusCode == 200) {
		r.getEntity().getContent();
	    } else {
		throw new RuntimeException(
			"Couldn't get the file because some error happened! Status code: " + statusCode);
	    }
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}
	return null;
    }

    /**
     * Decides if a Jenkins job exists with the name jobName.
     *
     * @param jobName
     *            name of a job
     */
    public boolean isExistingJob(String jobName) {
	HttpURLConnection conn;
	int statusCode = 0;
	try {
	    conn = (HttpURLConnection) new URL(jenkinsURL + "job/" + jobName).openConnection();
	    statusCode = conn.getResponseCode();
	} catch (MalformedURLException e) {
	    throw new IllegalArgumentException("The URL created by the argument isn't in a valid URL format!");
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has occured!");
	}

	return statusCode == 200;
    }

    public void updateJob(Job job) {
	if (!isExistingJob(job.getName())) {
	    throw new IllegalArgumentException("There isn't a job with the name " + job.getName()
		    + "!\nYou should use the 'createJob(Job j)' method to create a new job");
	}
	byte[] configXML = job.toXML();
	Response r;
	int statusCode = 0;
	try {
	    r = Request.Post(jenkinsURL + "job/" + job.getName() + "/config.xml")
		    .bodyByteArray(configXML, 0, configXML.length, ContentType.APPLICATION_XML).execute();
	    statusCode = r.returnResponse().getStatusLine().getStatusCode();
	} catch (UnsupportedEncodingException e) {
	    throw new RuntimeException("Error during encoding the job's name!");
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}

	if (statusCode != 200) {
	    throw new RuntimeException("Some error has happened, the returned status code: " + statusCode);
	}
    }

    public void performBuild(String jobName) {
	if (!isExistingJob(jobName)) {
	    throw new IllegalArgumentException("There isn't a job with name '" + jobName + "'!");
	}

	Response r;
	int statusCode = 0;
	try {
	    r = Request.Post(jenkinsURL + "job/" + jobName + "/build").execute();
	    statusCode = r.returnResponse().getStatusLine().getStatusCode();
	} catch (UnsupportedEncodingException e) {
	    throw new RuntimeException("Error during encoding the job's name!");
	} catch (IOException e) {
	    throw new RuntimeException("Some IO error has happened!");
	}

	if (statusCode != 201) {
	    throw new RuntimeException("Some error has happened, the returned status code: " + statusCode);
	}
    }

}
