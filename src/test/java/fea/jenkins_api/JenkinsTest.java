package fea.jenkins_api;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import fea.jenkins_api.jenkins.Jenkins;
import fea.jenkins_api.job.Job;

public class JenkinsTest {
    private Job job;
    private Jenkins jenkins;
    private String url = "http://localhost:8080/jenkins";
    String uid;
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @After
    public void clean() {
	jenkins = null;
	job = null;
    }

    @Test
    public void testJenkinsShouldWorkWhenURLIsOk() {
	jenkins = new Jenkins(url);
    }

    @Test
    public void testJenkinsShouldThrowExceptionWhenURLIsntInValidFormat() {
	expectedEx.expect(IllegalArgumentException.class);
	expectedEx.expectMessage("The argument jenkinsURLStr is not in a valid URL format!");
	jenkins = new Jenkins("alma");
    }

    @Test
    public void testJenkinsShouldThrowExceptionWhenURLIsNotJenkinsURL() {
	expectedEx.expect(IllegalArgumentException.class);
	expectedEx.expectMessage("The argument jenkingURLStr is not an URL of a Jenkins server!");
	jenkins = new Jenkins("http://www.google.com");
    }

    @Test
    public void testCreateJobShouldWorkWhenTheJobIsntExisting() {
	jenkins = new Jenkins(url);
	while (jenkins.isExistingJob((uid = java.util.UUID.randomUUID().toString()))) {
	}

	Job job = new Job(uid);
	job.getGit().setRepoURL("https://github.com/CymChad/BaseRecyclerViewAdapterHelper.git");
	job.getGit().setBranch("master");
	job.getBuildDiscarder().setNumToKeep(10);
	job.getFilter().setHardFilter("-^(.*/)?([Tt]est?)/.*");
	job.getTrigger().getPeriodicBuild().setSchedule("H/5 * * * *");
	jenkins.createJob(job);

	assertTrue(jenkins.isExistingJob(uid));

    }

    @Test
    public void testCreateJobShouldntWorkWhenJobIsExisting() {
	expectedEx.expect(RuntimeException.class);

	jenkins = new Jenkins(url);
	while (jenkins.isExistingJob((uid = java.util.UUID.randomUUID().toString()))) {
	}
	job = new Job(uid);
	jenkins.createJob(job);
	jenkins.createJob(job);
    }

    /*
     * public void testGetNumberOfIdleExecutors() { fail("Not yet implemented"); }
     * 
     * public void testGetNumberOfItemsInQueue() { fail("Not yet implemented"); }
     * 
     * public void testGetRunningJobURL() { fail("Not yet implemented"); }
     * 
     * public void testCreateJobStringString() { fail("Not yet implemented"); }
     * 
     * public void testCreateJobJob() { fail("Not yet implemented"); }
     * 
     * public void testDeleteJob() { fail("Not yet implemented"); }
     * 
     * public void testGetFileFromJobWorkspace() { fail("Not yet implemented"); }
     * 
     * public void testUpdateJob() { fail("Not yet implemented"); }
     * 
     * public void testPerformBuild() { fail("Not yet implemented"); }
     */
}
