package fea.jenkins_api;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import fea.jenkins_api.job.Job;

public class JobTest {

    private Job job;
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @After
    public void clean() {
	job = null;
    }

    @Test
    public void testJobShouldWorkWithNormalName() {
	job = new Job("test");
	assertThat(job.getName(), is("test"));
    }

    @Test
    public void testJobShouldThrowExceptionWhenEmptyStringNameIsGiven() {
	expectedEx.expect(IllegalArgumentException.class);
	expectedEx.expectMessage("The " + "name" + " string can't be null or empty string!");
	job = new Job("");
    }

    @Test
    public void testJobShouldThrowExceptionWhenNullNameIsGiven() {
	expectedEx.expect(IllegalArgumentException.class);
	expectedEx.expectMessage("The " + "name" + " string can't be null or empty string!");
	job = new Job(null);
    }

}
