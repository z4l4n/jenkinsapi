JenkinsAPI

<groupId>fea</groupId>
<artifactId>jenkins-api</artifactId>
<version>0.0.1-SNAPSHOT</version>


Jenkins class:
A Jenkins osztály reprezentálja a Jenkins szervert. Ennek konstruktorában kell megadni a Jenkins szerver URL címét String-ként (protocol://hostname:port/path). 
Ez ellenőrzésre kerül a konstruktorban (valid URL formátum-e, ha igen, tényleg Jenkins szerver URL-je-e). Ha sikeres a konstruálás, használhatjuk a Jenkins specifikus metódusokat.

Metódusok:
- int getNumberOfIdleExecutors(): visszaadja a tétlen executor-ok számát
- int getNumberOfItemsInQueue(): visszaadja a queue-ban lévő job-ok számát
- List<String>getRunningJobURL(): visszaadja az éppen build-elés alatt álló job-ok URL-jét String listában. Ha nincs ilyen, akkor üres listát ad vissza
- void createJob(String jobName, String configXMLPath): készít egy jobName nevű job-ot a Jenkins szerveren, melynek tulajdonságait a configXMLPath által megadott útvonalon lévő fájl tartalmazza (ennek config.xml fájlnak kell lennie)
- void createJob(Job job): készít egy job-ot a Jenkins szerveren, melynek tulajdonságai a Job osztály (lásd lejjebb) setter függvényei által vannak megadva
- void deleteJob(String jobName): törli a Jenkins szerverről a jobName nevű job-ot, ha létezik ilyen, különben IllegalArgumentException-t dob
- InputStream getFileFromJobWorkspace(String jobName, String pathName): Az jobName nevű job workspace mappájához viszonyítva a pathName által adott file-t adja vissza InputStream-ként
- void updateJob(Job job): update-eli a job config.xml-jét a Jenkins szerveren. Csak a szerveren létező job-ra használhatjuk, különben IllegalArgumentException-t dob
- void performBuild(String jobName): elindítja a build-elését a jobName nevű job-nak, ha létezik, különben IllegalArgumentException-t dob



Job class:
A Job osztály egy Jenkins (FrontEndART SourceMeter Plugin típusú) job-ot  reprezentál. A konstruktorban a job azonosító nevét kell megadni String-ként. A job további beállításai a metódusain keresztül kell elvégezni
A beállítások menü csoportjait alosztályok(alobjektumok) tartalmazzák.

Metódusok:
- String getName() : visszaadja a job nevét
- Git getGit(): visszaadja a job-hoz tartozó Git objektumot, ezen git-tel kapcsolaton beállításokat hajthatunk végre (repoURL, branch, authentication, stb)
- BuildDiscarder getBuildDiscarder(): visszaadja a job-hoz tartozó BuildDiscarder objektumot, ezen a régi build-ek törlésével kapcsolatos beállításokat hajthatunk végre (hány napig tartsa meg, hány db-ot tartson meg, stb.)
- Filter getFilter(): visszaadja a job-hoz tartozó BuildDiscarder objektumot, ezen a filter beállításokat végezhetjük el (hard filter, soft filter)
- Trigger getTrigger(): visszaadja a job-hoz tartozó Trigger objektumot, ezen a trigger beállításokat állíthatjuk (periodic build, schedule)
- void setContinueBuildIfFailed(boolean continueBuildIfFailed) : hiba esetén is folytassa-e a build-elést
- setVulnerabilityCheck(boolean vulnerabilityCheck): VulnerabilityHunter modul használatának ki-be kapcsolása
- void setEnabled(boolean enabled): job (újabb build-ek készítésének) ki-be kapcsolása
- void setDescription(String description): job leírás beállítása
- void clearFilter()
- void clearBuildDiscarder()
- void clearDescription(): Ezek törlik ("checkbox-t disabled-re állítja") az adott opciókat
- byte[] toXML(): byte tömbként legenerálja a config.xml fájlt, ezt használja a Jenkins szerver a job létrehozásához



Tipikus használati eset:

Jenkins jenkins = new Jenkins("http://smdb-dev-analyzer-02:8080/");
Job job = new Job("jobname");
job.getGit().setRepoURL("https://github.com/CymChad/BaseRecyclerViewAdapterHelper.git");
job.getGit().setBranch("master");
job.getBuildDiscarder().setNumToKeep(10);
job.getFilter().setHardFilter("-^(.*/)?([Tt]est?)/.*");
job.getTrigger().getPeriodicBuild().setSchedule("H/5 * * * *");
jenkins.createJob(job);	




